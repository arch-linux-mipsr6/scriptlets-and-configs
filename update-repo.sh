#!/bin/bash

SIGNING_KEY='F5F01FAC29FC3BBFC43444D02FFF31567A0B462B'
LOCAL_REPO_PATH='/home/hachiroku/arch-mipsr6/packages/repo'
REMOTE_REPO_HOST='archmips.local.nekomimi.link'
REMOTE_REPO_PATH='/srv/archmips'

REPO_ADD='/usr/bin/repo-add'
RSYNC='/usr/bin/rsync'

pushd "${LOCAL_REPO_PATH}"

for repo in core extra community upstream
do
  pushd "${repo}"
  rm "${repo}".{db,files}*  
  ${REPO_ADD} "${repo}.db.tar.gz" *.pkg.tar.zst -R -s -k ${SIGNING_KEY}
  popd
done

popd

${RSYNC} -av --delete-after "${LOCAL_REPO_PATH}/" ${REMOTE_REPO_HOST}:"${REMOTE_REPO_PATH}/"
