#!/bin/bash

CIEL_INSTALL_DIR='/var/lib/ciel'
CIEL='/usr/bin/ciel'
SUDO=''

INSTANCE_NAME='arch-test'
MAKEPKG_USER='build'
PACKAGE_NAME=''
MAKEPKG_OPTS=''

MAKEPKG_CONF='/home/hachiroku/arch-mipsr6/packages/makepkg.conf'
MAKEPKG_TREE='/home/hachiroku/arch-mipsr6/packages/pkgbuilds'
OUTPUT_TREE='/home/hachiroku/arch-mipsr6/packages/repo'

if (( ${EUID} != 0 )); then
  SUDO='/usr/bin/sudo'
fi

while getopts ":c:i:m:o:O:p:u:" options
do
  case "${options}" in
    c)
      MAKEPKG_CONF="${OPTARG}"
      ;;
    i)
      INSTANCE_NAME="${OPTARG}"
      ;;
    m)
      MAKEPKG_TREE="${OPTARG}"
      ;;
    o)
      OUTPUT_TREE="${OPTARG}"
      ;;
    O)
      MAKEPKG_OPTS="${OPTARG}"
      ;;
    p)
      PACKAGE_NAME="${OPTARG}"
      ;;
    u)
      MAKEPKG_USER="${OPTARG}"
      ;;
    :)
      echo "Missing required argument."
      exit -1
      ;;
    *)
      echo "Invalid argument."
      exit -1
      ;;
  esac
done

pushd "${CIEL_INSTALL_DIR}"

${SUDO} ${CIEL} shell \
  -i ${INSTANCE_NAME} \
  "pacman -Syyu"

${SUDO} touch "${CIEL_INSTALL_DIR}"/TREE/makepkg.conf
${SUDO} mount -o ro,bind "${MAKEPKG_CONF}" "${CIEL_INSTALL_DIR}"/TREE/makepkg.conf
${SUDO} mount -o ro,bind "${MAKEPKG_TREE}" "${CIEL_INSTALL_DIR}"/TREE/pkgbuilds
${SUDO} mount -o bind "${OUTPUT_TREE}" "${CIEL_INSTALL_DIR}"/OUTPUT/debs

${SUDO} ${CIEL} shell \
  -i ${INSTANCE_NAME} \
  "cd /tree/pkgbuilds/${PACKAGE_NAME} && \
  sudo -u ${MAKEPKG_USER} makepkg -Ccis \
  --config /tree/makepkg.conf ${MAKEPKG_OPTS}"

${SUDO} umount -f -l "${CIEL_INSTALL_DIR}"/TREE/makepkg.conf
${SUDO} umount -f -l "${CIEL_INSTALL_DIR}"/TREE/pkgbuilds
${SUDO} umount -f -l "${CIEL_INSTALL_DIR}"/OUTPUT/debs

popd
